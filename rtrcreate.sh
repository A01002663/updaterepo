#!/bin/bash
name=router

VBoxManage createvm --name $name  --ostype "RedHat_64" --register
VBoxManage modifyvm $name --memory 4096 --vram 128

VBoxManage storagectl $name --name "IDE Controller" --add ide
VBoxManage storageattach $name --storagectl "IDE Controller" --port 0 --device 0 --type dvddrive --medium /home/achahal/scripts/test/works.iso
VBoxManage storageattach $name --storagectl "IDE Controller" --port 0 --device 1 --type dvddrive --medium /usr/share/virtualbox/VBoxGuestAdditions.iso

VBoxManage createhd --filename ~/VirtualBox\ VMs/$name/$name.vdi --size 20480

VBoxManage storageattach $name --storagectl "IDE Controller" --port 1 --device 0 --type hdd --medium ~/VirtualBox\ VMs/$name/$name.vdi


#VBoxManage modifyvm $name --nic1 bridged --bridgeadapter1 vlan2016 --nicpromisc1 allow-all --nictype1 virtio
VBoxManage modifyvm $name --nic1 nat --nicpromisc1 allow-all --nictype1 virtio

# VBoxManage modifyvm $name --nic2 intnet --intnet1 as_net --nicpromisc2 allow-all --nictype2 virtio

VBoxManage modifyvm $name --macaddress1 auto
VBoxManage modifyvm $name --macaddress2 auto

VBoxManage startvm router

sleep 5

VBoxManage modifyvm router --nic1 bridged --bridgeadapter1 VLAN2016 --nicpromisc1 allow-all --nictype1 virtio && VBoxManage modifyvm router --nic2 intnet --intnet1 as_net --nicpromisc2 allow-all --nictype2 virtio 2>&1 /dev/null
myvar=$?
while [ $myvar != 0 ]

do
  sleep 10
  VBoxManage modifyvm router --nic1 bridged --bridgeadapter1 VLAN2016 --nicpromisc1 allow-all --nictype1 virtio && VBoxManage modifyvm router --nic2 intnet --intnet1 as_net --nicpromisc2 allow-all --nictype2 virtio 2>&1 >/dev/null
  myvar=$?


done
sleep 5
echo "Transferring scripts now."
vboxmanage startvm router

until [[ $pingvar == 0 ]]
do
	ping -c 1 10.16.255.1
	pingvar=$?
	echo "pinging now"
done
echo "loop ended"
scp -rp ~/scripts/test/updaterepo/ achahal@10.16.255.1:/home/achahal/

ssh achahal@10.16.255.1
ssh achahal@10.16.255.1 'bash /home/achahal/updaterepo/execute.sh'
