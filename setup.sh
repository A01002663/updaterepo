#!/bin/bash - 

yum -y update
yum -y group install base

yum -y install epel-release
yum -y update

yum -y install curl vim wget tmux nmap-ncat tcpdump nmap git

yum -y install kernel-devel kernel-headers dkms gcc gcc-c+

setenforce 0
sed -r -i 's/SELINUX=(enforcing|disabled)/SELINUX=permissive/' /etc/selinux/config

systemctl stop NetworkManager.service
systemctl disable NetworkManager.service

systemctl stop firewalld.service
systemctl disable firewalld.service

systemctl enable network.service 
systemctl start network.service

systemctl enable sshd.service
systemctl start sshd.service

