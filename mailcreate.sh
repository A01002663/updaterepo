#!/bin/bash

name=mail

vboxmanage createvm --name $name --ostype "RedHat_64" --register
vboxmanage modifyvm $name --memory 4096 --vram 128

vboxmanage storagectl $name --name "IDE Controller" --add ide
vboxmanage storageattach $name --storagectl "IDE Controller" --port 0 --device 0 --type dvddrive --medium /home/achahal/scripts/test/works.iso
vboxmanage storageattach $name --storagectl "IDE Controller" --port 0 --device 1 --type dvddrive --medium /usr/share/virtualbox/VBoxGuestAdditions.iso

vboxmanage createhd --filename ~/VirtualBox\ VMs/$name/$name.vdi --size 20480

vboxmanage storageattach $name --storagectl "IDE Controller" --port 1 --device 0 --type hdd --medium ~/VirtualBox\ VMs/$name/$name.vdi

vboxmanage modifyvm $name --nic1 nat --nicpromisc1 allow-all --nictype2 virtio

vboxmanage modifyvm $name --macaddress1 0800277228AC
VBoxManage startvm mail

sleep 5

VBoxManage modifyvm mail --nic1 intnet --intnet1 as_net --nicpromisc1 allow-all --nictype1 virtio

myvar=$?
while [ $myvar != 0 ]

do
  sleep 10
  VBoxManage modifyvm mail --nic1 intnet --intnet1 as_net --nicpromisc1 allow-all --nictype1 virtio
  myvar=$?

done
vboxmanage startvm mail
echo "VM Creation Complete"
